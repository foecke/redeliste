import tkinter as tk
import tkinter.ttk as ttk

import tkinter.filedialog as fd
import tkinter.simpledialog as sd
import tkinter.messagebox as mb

import csv


class App:
        
    def __init__(self, root):
        self.root = root
        self.root.protocol("WM_DELETE_WINDOW", self._quit_app)
        
        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)
        
        self.style = ttk.Style()
        self.style.configure("speakerlist.Treeview", rowheight=40, highlightthickness=1, font=('TkDefaultFont', 18))
        self.style.configure("speakerlist.Treeview.Heading", rowheight=30, font=('TkDefaultFont', 12))
        self.style.configure("participantlist.Treeview", rowheight=30, highlightthickness=1, font=('TkDefaultFont', 12))
        self.style.configure("participantlist.Treeview.Heading", rowheight=30, font=('TkDefaultFont', 12))
        self.style.configure("borderless.TLabelframe", borderwidth=0)
        self.style.configure("TLabelframe.Label", font=('TkDefaultFont', 12, 'bold'))
        self.style.configure("TButton", font=('TkDefaultFont', 12, 'bold'))
        
        self.menu_file = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="Datei", menu=self.menu_file)
        self.menu_file.add_command(label="Lade Teilnehmerliste ...", command=self._participantlist_load_action, accelerator="Strg+O")
        self.root.bind('<Control-o>', lambda event: self._participantlist_load_action()) # open participant list
        self.menu_file.add_command(label="Speichere aktuelle Teilnehmerliste", command=self._participantlist_save_action, accelerator="Strg+S")
        self.root.bind('<Control-s>', lambda event: self._participantlist_save_action()) # save participant list
        # self.menu_file.add_command(label="Lade letzte Teilnehmer")
        self.menu_file.add_separator()
        self.menu_file.add_command(label="Beenden", command=self._quit_app, accelerator="Alt+F4")
        
        # self.menu_edit = tk.Menu(self.menu, tearoff=0)
        # self.menu.add_cascade(label="Bearbeiten", menu=self.menu_edit)
        # self.menu_edit.add_checkbutton(label="Harte quota")
        
        self.menu_view = tk.Menu(self.menu, tearoff=0)
        self.menu.add_cascade(label="Ansicht", menu=self.menu_view)
        # self.menu_view.add_checkbutton(label="Zeige Timer")
        # self.menu_view.add_separator()
        self.window_layout = tk.IntVar()
        self.menu_view.add_radiobutton(label="Horizontale Ansicht", command=self._window_set_layout_horizontal_action, variable=self.window_layout, value=0)
        self.menu_view.add_radiobutton(label="Vertikale Ansicht", command=self._window_set_layout_vertical_action, variable=self.window_layout, value=1)
        
        # self.menu_help = tk.Menu(self.menu, tearoff=0)
        # self.menu.add_cascade(label="Hilfe", menu=self.menu_help)
        # self.menu_help.add_command(label="Hilfe")
        # self.menu_help.add_command(label="Info")
        
        
        
        self.frame_current_speaker = ttk.Labelframe(self.root, text="Aktuell spricht:", style="borderless.TLabelframe")
        
        self.frame_current_speaker.columnconfigure(0, weight=2)
        self.frame_current_speaker.columnconfigure(1, weight=0)
        self.frame_current_speaker.columnconfigure(2, weight=0)
        
        self.current_speaker = tk.StringVar()
        self.current_speaker.set("---")
        self.current_speaker_quota = tk.StringVar()
        self.current_speaker_quota.set("-")
        self.current_speaker_label = tk.Label(self.frame_current_speaker, textvariable=self.current_speaker, font=('TkDefaultFont', 24, 'bold'))
        self.current_speaker_label.grid(row=0, column=0, sticky='nsew', ipadx=10, ipady=10)
        self.current_speaker_quota_label = tk.Label(self.frame_current_speaker, textvariable=self.current_speaker_quota, font=('TkDefaultFont', 12, 'bold'))
        self.current_speaker_quota_label.grid(row=0, column=1, sticky='nsew', ipadx=10)
        self.current_speaker_next_button = ttk.Button(self.frame_current_speaker, text='Next', command=self._speakerlist_next_action, style="style.TButton")
        self.current_speaker_next_button.grid(row=0, column=2, sticky='ew')
                
        self.frame_timer = tk.Frame(self.root)

        self.frame_timer.columnconfigure(0, weight=2)
        self.frame_timer.columnconfigure(1, weight=0)
        self.frame_timer.columnconfigure(2, weight=0)
        
        #self.timer = tk.Label(self.frame_timer, text='--:--')
        #self.timer.grid(row=0, column=0, sticky='nsew')
        #self.timer_start_stop_button = tk.Button(self.frame_timer, text='Start/Stop')
        #self.timer_start_stop_button.grid(row=0, column=1, sticky='nsew')
        #self.timer_reset_button = tk.Button(self.frame_timer, text='Reset')
        #self.timer_reset_button.grid(row=0, column=2, sticky='nsew')
        
        
        
        self.frame_speakerlist = ttk.LabelFrame(self.root, text="Redeliste:", style="borderless.TLabelframe")
        
        self.frame_speakerlist.columnconfigure(0, weight=2)
        self.frame_speakerlist.columnconfigure(1, weight=0)
        self.frame_speakerlist.columnconfigure(2, weight=0)
        self.frame_speakerlist.rowconfigure(0, weight=1)
        
        self.speakerlist = ttk.Treeview(self.frame_speakerlist, columns=('Position', 'Name', 'Quotiert'), show='headings', style="speakerlist.Treeview")
        self.speakerlist.heading('#1', text='Position')
        self.speakerlist.heading('#2', text='Name')
        self.speakerlist.heading('#3', text='Quotiert')
        self.speakerlist.column('#1', stretch=tk.NO, width=80)
        self.speakerlist.column('#2', stretch=tk.YES, minwidth=150)
        self.speakerlist.column('#3', stretch=tk.NO, width=80)
        
        self.speakerlist.bind('<Delete>', lambda event: self._speakerlist_remove_action())
        
        self.speakerlist_sb = ttk.Scrollbar(orient="vertical", command=self.speakerlist.yview)
        self.speakerlist.configure(yscrollcommand=self.speakerlist_sb.set)
        self.speakerlist.grid(row=0, column=0, columnspan=3, sticky='nsew')
        self.speakerlist_sb.grid(row=0, column=4, sticky='ns', in_=self.frame_speakerlist)

        self.speakerlist_button_remove_selection = tk.Button(self.frame_speakerlist, text="", command=self._speakerlist_remove_selection_action)
        self.speakerlist_button_remove_selection.grid(row=2, column=0, sticky='nsew')
        self.speakerlist_button_remove = tk.Button(self.frame_speakerlist, text='zurückziehen', command=self._speakerlist_remove_action)
        self.speakerlist_button_remove.grid(row=2, column=1, sticky='nsew')
        self.speakerlist_button_empty = tk.Button(self.frame_speakerlist, text='Liste leeren', command=self._speakerlist_empty_action)
        self.speakerlist_button_empty.grid(row=2, column=2, sticky='nsew')
        
        self.speakerlist.tag_configure('quotiert', background='orange')
        
        self.speakerlist.bind('<Button-1>', lambda event: self._speakerlist_mousepress_action(event))
        self.speakerlist.bind('<B1-Motion>', lambda event: self._speakerlist_mousemotion_action(event))
        self.speakerlist.bind('<ButtonRelease-1>', lambda event: self._speakerlist_mouserelease_action(event))
        
        self.speakerlist_visible_elements = []
        self.speakerlist_label_dragdrop_name = tk.StringVar()        
        self.speakerlist_label_dragdrop = tk.Label(self.frame_speakerlist, textvariable=self.speakerlist_label_dragdrop_name, font=('TkDefaultFont', 14, 'bold'), pady=2, borderwidth=1, relief="groove")
        
        
        
        self.frame_participantlist = ttk.LabelFrame(self.root, text="Anwesende:", style="borderless.TLabelframe")
             
        self.frame_participantlist.columnconfigure(0, weight=3)
        self.frame_participantlist.columnconfigure(1, weight=1)
        self.frame_participantlist.columnconfigure(2, weight=0)
        self.frame_participantlist.rowconfigure(0, weight=1)
        self.frame_participantlist.rowconfigure(1, weight=0)
        self.frame_participantlist.rowconfigure(2, weight=0)
        
        self.search_var = tk.StringVar()
        # self.search_var.trace("w", lambda name, index, mode: self.selected)
        #self.entry = tk.Entry(self.frame_participantlist, textvariable=self.search_var)
        #self.entry.grid(row=0, column=0, sticky='nsew')
        #self.searchbtn = tk.Button(self.frame_participantlist, text='Suchen')
        #self.searchbtn.grid(row=0, column=1, sticky='nsew')
        
        self.participantlist = ttk.Treeview(self.frame_participantlist, columns=('Name', 'Quotiert'), show='headings', style="participantlist.Treeview")
        self.participantlist.heading('#1', text='Name')
        self.participantlist.heading('#2', text='Quotiert')
        self.participantlist.column('#1', stretch=tk.YES, minwidth=150)
        self.participantlist.column('#2', stretch=tk.NO, width=80)
        
        self.participantlist.bind('<Delete>', lambda event: self._participantlist_remove_action())
        self.participantlist.bind("<Double-1>", self._participantlist_doubleclick_action)
        self.participantlist.bind("<Button-3>", self._participantlist_rightclick_action)

        self.participantlist_sb = ttk.Scrollbar(orient="vertical", command=self.participantlist.yview)
        self.participantlist.configure(yscrollcommand=self.participantlist_sb.set)
        self.participantlist.grid(row=0, column=0, columnspan=2, sticky='nsew')
        self.participantlist_sb.grid(row=0, column=2, sticky='ns', in_=self.frame_participantlist)
        
        self.participantlist_contextmenu = tk.Menu(self.frame_participantlist, tearoff = 0)
        self.participantlist_contextmenu_quota = tk.IntVar()
        self.participantlist_contextmenu.add_command(label="Redebeitrag hinzufügen", command=self._speakerlist_add_selected_action)
        self.participantlist_contextmenu.add_separator()
        self.participantlist_contextmenu.add_command(label="Namen bearbeiten", command=self._participantlist_contextmenu_name_update)
        self.participantlist_contextmenu.add_checkbutton(label="quotiert", onvalue=1, offvalue=0, variable=self.participantlist_contextmenu_quota, command=self._participantlist_contextmenu_quota_update)
        
        self.speakerlist_button_add = tk.Button(self.frame_participantlist, text='Redebeitrag hinzufügen', command=self._speakerlist_add_selected_action)
        self.speakerlist_button_add.grid(row=2, column=0, sticky='nsew')
        self.participantlist_button_remove = tk.Button(self.frame_participantlist, text='Person löschen', command=self._participantlist_remove_action)
        self.participantlist_button_remove.grid(row=2, column=1, sticky='nsew')
        
        
        
        self.frame_add_participants = ttk.LabelFrame(self.root, text="Person hinzufügen:", style="borderless.TLabelframe")
        
        self.frame_add_participants.columnconfigure(0, weight=3)
        self.frame_add_participants.columnconfigure(1, weight=1)
        self.frame_add_participants.columnconfigure(2, weight=1)
        
        self.add_participant_name = tk.StringVar()
        self.add_participant_entry = tk.Entry(self.frame_add_participants, textvariable=self.add_participant_name)
        self.add_participant_entry.grid(row=0, column=0, rowspan=2, sticky='ew', ipady=2)
        self.add_participant_entry.bind('<Return>', lambda event: self._participantlist_add_action())
        self.add_participant_entry.bind('<Control_L>', lambda event: print("hey"))
        
        self.add_participant_quota = tk.IntVar()
        self.add_participant_radio_quota = tk.Radiobutton(self.frame_add_participants, text="quotiert", variable=self.add_participant_quota, value=1)
        self.add_participant_radio_no_quota = tk.Radiobutton(self.frame_add_participants, text="nicht quotiert", variable=self.add_participant_quota, value=0)
        self.add_participant_radio_quota.grid(row=0, column=1)
        self.add_participant_radio_no_quota.grid(row=1, column=1)
   
        self.participantlist_button_add = tk.Button(self.frame_add_participants, text='Hinzufügen', command=self._participantlist_add_action)
        self.participantlist_button_add.grid(row=0, column=2, rowspan=2, sticky='ew')
        
        
        self._window_set_layout_vertical_action()
        
        
        
    def _speakerlist_get_speakerlist_visible_elements(self):
        speakerlist_visible_elements = []
        for current_item in self.speakerlist.get_children():
            try: 
                self.speakerlist.bbox(current_item)[3]
                speakerlist_visible_elements.append(current_item)
            except:
                pass
        return [[speakerlist_visible_elements[0], speakerlist_visible_elements[-1]], self.speakerlist.bbox(speakerlist_visible_elements[0])[1], self.speakerlist.bbox(speakerlist_visible_elements[-1])[1] + self.speakerlist.bbox(speakerlist_visible_elements[-1])[3]]
        
    
    def _speakerlist_mousepress_action(self, event):
        current_item = self.speakerlist.identify_row(event.y)
        if current_item:
            self.draggedItem = current_item
            self.speakerlist_label_dragdrop_name.set(self.speakerlist.item(self.draggedItem)["values"][1])
            self.speakerlist_visible_elements = self._speakerlist_get_speakerlist_visible_elements()
       
        
    def _speakerlist_mousemotion_action(self, event):
        if self.speakerlist_visible_elements:
            if event.y <= self.speakerlist_visible_elements[1]:
                if event.y <= self.speakerlist_visible_elements[1]-50:
                    # print("scroll: up")
                    self.speakerlist.yview_scroll(-1, "units")
                    self.speakerlist_visible_elements = self._speakerlist_get_speakerlist_visible_elements()
                current_item = self.speakerlist_visible_elements[0][0]       
                item_x, item_y, item_w, item_h = self.speakerlist.bbox(current_item) 
                self.speakerlist_label_dragdrop.place(x=item_x, y=item_y-0.5*item_h+5, width=item_w)
                
            elif event.y >= self.speakerlist_visible_elements[2]:
                if event.y >= self.speakerlist_visible_elements[2]+50:
                    # print("scroll: down")
                    self.speakerlist.yview_scroll(1, "units")
                    self.speakerlist_visible_elements = self._speakerlist_get_speakerlist_visible_elements()
                current_item = self.speakerlist_visible_elements[0][1]       
                item_x, item_y, item_w, item_h = self.speakerlist.bbox(current_item) 
                self.speakerlist_label_dragdrop.place(x=item_x, y=item_y+0.5*item_h+5, width=item_w)
            else:        
                current_item = self.speakerlist.identify_row(event.y)
                if current_item:
                    item_x, item_y, item_w, item_h = self.speakerlist.bbox(current_item) 
                    self.speakerlist_label_dragdrop.place(x=item_x, y=item_y-0.5*item_h+5, width=item_w)
            
        
    def _speakerlist_mouserelease_action(self, event):
        if self.speakerlist_visible_elements:
            if event.y <= self.speakerlist_visible_elements[1]:
                current_item = self.speakerlist_visible_elements[0][0]
                if current_item:
                    move_to_index = self.speakerlist.index(current_item)             
            elif event.y >= self.speakerlist_visible_elements[2]:
                current_item = self.speakerlist_visible_elements[0][1]
                if current_item:
                    move_to_index = self.speakerlist.index(current_item)+1
            else:
                current_item = self.speakerlist.identify_row(event.y)
                if current_item:
                    move_to_index = self.speakerlist.index(current_item)        
            if current_item:
                if self.speakerlist.index(self.draggedItem) < move_to_index:
                    move_to_index = move_to_index-1
                self.speakerlist.move(self.draggedItem, '', move_to_index)            
            self.speakerlist_label_dragdrop.place_forget()
            self._speakerlist_set_position_action()
            self.root.after(1000,lambda: self._speakerlist_remove_selection_action())
            
                
        
    def _quit_app(self):
        answer = mb.askyesno(title='Beenden?', message='Anwendung beenden?')
        if answer:
            self.root.destroy()   
            
            
    def add_participant(self, name, add_participant_quota):
        self.participantlist.insert('', 'end', values=[name, add_participant_quota])
        self.add_participant_entry.delete(0, 'end')
        
    
    def _participantlist_load_action(self):
        filename = fd.askopenfilename()
        if filename:
            answer = mb.askyesno(title='Lade Teilnehmerliste aus Datei?', message='Aktuelle Teilnehmerliste überschreiben?')
            if answer:
                self._participantlist_empty_action()
                with open(filename, newline='') as csvfile:
                    reader = csv.DictReader(csvfile)
                    for row in reader:
                        self.add_participant(row['name'], row['quotiert'])
    
    
    def _participantlist_save_action(self):
        filename = fd.asksaveasfile(mode='w', defaultextension=".txt")
        if filename is None:
            return
        with open(filename.name, 'w', newline='') as csvfile:
            fieldnames = ['name', 'quotiert']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()        
            for currentItem in self.participantlist.get_children():
                writer.writerow({'name': self.participantlist.item(currentItem)["values"][0], 'quotiert': self.participantlist.item(currentItem)["values"][1]})
                
    
    def _participantlist_empty_action(self):
        for item in self.participantlist.get_children():
            self.participantlist.delete(item)            
                
            
    def _participantlist_add_action(self):
        if self.add_participant_name.get():
            self.add_participant(f'{self.add_participant_name.get()}', self.add_participant_quota.get())
        
        
    def _participantlist_remove_action(self):
        if self.participantlist.selection():
            selected_item = self.participantlist.selection()[0]
            self.participantlist.delete(selected_item)
        
        
    def _participantlist_doubleclick_action(self, event):
        self._speakerlist_add_selected_action()
        
        
    def _participantlist_rightclick_action(self, event):
        selected_item = self.participantlist.identify_row(event.y)
        if selected_item:
            self.participantlist.selection_set(selected_item)
            self.participantlist_contextmenu_quota.set(self.participantlist.item(selected_item)["values"][1])
            self.participantlist_contextmenu.tk_popup(event.x_root, event.y_root)
        else:
            pass
        
        
    def _participantlist_contextmenu_quota_update(self):  
        if self.participantlist.selection():
            selected_item = self.participantlist.selection()[0]
            self.participantlist.set(selected_item, 1, self.participantlist_contextmenu_quota.get())
            
            
    def _participantlist_contextmenu_name_update(self):  
        if self.participantlist.selection():
            selected_item = self.participantlist.selection()[0]
            answer = sd.askstring("Name bearbeiten", "Neuer Name:", initialvalue=(self.participantlist.item(selected_item)["values"][0]))
            if answer:
                self.participantlist.set(selected_item, 0, answer)
                
                
    def _speakerlist_add_selected_action(self):
        if self.participantlist.selection():
            selected_item = self.participantlist.selection()[0]
            name = self.participantlist.item(selected_item)["values"][0]
            #print(self.participantlist.item(selected_item)["values"][0])
            if(self._speakerlist_add_is_allowed_action(name)):
                quota = self.participantlist.item(selected_item)["values"][1]
                if(quota == 1):
                    new_item = self.speakerlist.insert('', 'end', text=self.participantlist.item(selected_item)["text"], values=[0] + self.participantlist.item(selected_item)["values"], tags=('quotiert'))
                    print("hinzugefügte Person ist quotiert")
                    self._speakerlist_fix_quota_action(new_item)
                else:
                    new_item = self.speakerlist.insert('', 'end', text=self.participantlist.item(selected_item)["text"], values=[0] + self.participantlist.item(selected_item)["values"])
                self.speakerlist.selection_set(new_item)
                self._speakerlist_set_position_action()
                self.root.after(1000,lambda: self._speakerlist_remove_selection_action())


    def _speakerlist_add_is_allowed_action(self, name_new_speaker):
       #print(self.current_speaker.get())
       if(self.current_speaker.get() == name_new_speaker):
           return False
       for current_item in self.speakerlist.get_children():
           name = self.speakerlist.item(current_item)["values"][1]
           if(name == name_new_speaker):
               return False
           if(self.speakerlist.next(current_item) == ''):
               return True
       return True


    def _speakerlist_next_action(self):
        if self.speakerlist.get_children():
            selected_item = self.speakerlist.get_children()[0]
            self.current_speaker.set(self.speakerlist.item(selected_item)["values"][1])
            quota = self.speakerlist.item(selected_item)["values"][2]
            if (quota == 0):
                self.current_speaker_quota.set("nicht\nquotiert")
            else:
                self.current_speaker_quota.set("quotiert")
            self.speakerlist.delete(selected_item)
        else:
            self.current_speaker.set("---")
            self.current_speaker_quota.set("-")
        self._speakerlist_set_position_action()


    def _speakerlist_remove_selection_action(self):
        self.speakerlist.selection_remove(self.speakerlist.selection())


    def _speakerlist_remove_action(self):
        if self.speakerlist.selection():
            selected_item = self.speakerlist.selection()[0]
            self.speakerlist.delete(selected_item)
            self._speakerlist_check_quota_action()
            self._speakerlist_set_position_action()
        
        
    def _speakerlist_empty_action(self):
        if self.speakerlist.get_children():
            selected_item = self.speakerlist.get_children()
            for child in selected_item:
                self.speakerlist.delete(child)


    def _speakerlist_check_quota_action(self):
        # check for first item
        if(self.current_speaker_quota.get() == "nicht\nquotiert"):
            first_item = self.speakerlist.get_children()[0]
            quota_first = self.speakerlist.item(first_item)["values"][2]
            if(quota_first == 0):
                print("Aktueller Speaker und erste Person auf der Redeliste nicht quotiert:")
                print(self.current_speaker.get() + " und " +
                      self.speakerlist.item(first_item)["values"][1])
                next_item = first_item
                while self.speakerlist.next(next_item) != '':
                    next_next_item = self.speakerlist.next(next_item)
                    q = self.speakerlist.item(next_next_item)["values"][2]
                    if (q == 1):
                        self.speakerlist.move(next_next_item, '', 0)
                        self.speakerlist.selection_add(next_next_item)
                        break
                    else:
                        next_item = next_next_item
        # iteration over all items
        for current_item in self.speakerlist.get_children():
            print(self.speakerlist.item(current_item)["values"])
            quota = self.speakerlist.item(current_item)["values"][2];
            if(quota == 0):
                next_item = self.speakerlist.next(current_item)
                if(next_item == ''):
                    break
                quota_next = self.speakerlist.item(next_item)["values"][2];
                if(quota_next == 0):
                    print("Zwei unqotierte aufeinanderfolgend:")
                    print(self.speakerlist.item(current_item)["values"][1]+" und "+self.speakerlist.item(next_item)["values"][1])
                    index = self.speakerlist.index(next_item)
                    while self.speakerlist.next(next_item) != '':
                        next_next_item = self.speakerlist.next(next_item)
                        q = self.speakerlist.item(next_next_item)["values"][2]
                        if(q == 1):
                            self.speakerlist.move(next_next_item,'',index)
                            self.speakerlist.selection_add(next_next_item)
                            break
                        else:
                            next_item = next_next_item


    def _speakerlist_fix_quota_action(self,quota_person):
        # check for first item
        if(self.current_speaker_quota.get() == "nicht\nquotiert" or self.current_speaker_quota.get() == "-"):
            first_item = self.speakerlist.get_children()[0]
            quota_first = self.speakerlist.item(first_item)["values"][2]
            if(quota_first == 0):
                print("Aktueller Speaker und erste Person auf der Redeliste nicht quotiert:")
                print(self.current_speaker.get() + " und " +
                      self.speakerlist.item(first_item)["values"][1])
                self.speakerlist.move(quota_person, '', 0)
                return
        # iteration over all items
        for current_item in self.speakerlist.get_children():
            print(self.speakerlist.item(current_item)["values"])
            quota = self.speakerlist.item(current_item)["values"][2]
            if(quota == 0):
                next_item = self.speakerlist.next(current_item)
                quota_next = self.speakerlist.item(next_item)["values"][2];
                if(quota_next == 0):
                    print("Zwei unqotierte aufeinanderfolgend:")
                    print(self.speakerlist.item(current_item)["values"][1]+" und "+self.speakerlist.item(next_item)["values"][1])
                    index = self.speakerlist.index(next_item)
                    self.speakerlist.move(quota_person,'',index)
                    break


    def _speakerlist_set_position_action(self):
        for current_item in self.speakerlist.get_children():
            position = self.speakerlist.index(current_item)+1
            self.speakerlist.set(current_item, 0, position)



    def _window_set_layout_vertical_action(self):
        
        root.geometry('600x800')
        root.minsize(400, 700)
        
        self.window_layout.set(1)
        
        self.root.columnconfigure(0, weight=1)    
        self.root.columnconfigure(1, weight=0)

        self.root.rowconfigure(0, weight=0)
        self.root.rowconfigure(1, weight=1)
        self.root.rowconfigure(2, weight=1)    
        self.root.rowconfigure(3, weight=0)
        
        self.frame_current_speaker.grid(row=0, column=0, sticky='nsew', padx=5, pady=5)        
        self.frame_speakerlist.grid(row=1, column=0, sticky='nsew', padx=5, pady=5)          
        self.frame_participantlist.grid(row=2, column=0, rowspan=1, sticky='nsew', padx=5, pady=5)           
        self.frame_add_participants.grid(row=3, column=0, columnspan=1, sticky='nsew', padx=5, pady=5)
        

    def _window_set_layout_horizontal_action(self):
        
        root.geometry("700x400")
        root.minsize(700, 400)
        
        self.window_layout.set(0)
        
        self.root.columnconfigure(0, weight=2)    
        self.root.columnconfigure(1, weight=1)

        self.root.rowconfigure(0, weight=0)
        self.root.rowconfigure(1, weight=1)
        self.root.rowconfigure(2, weight=0) 
        self.root.rowconfigure(3, weight=0)
        
        self.frame_current_speaker.grid(row=0, column=0, sticky='nsew', padx=5, pady=5)        
        self.frame_speakerlist.grid(row=1, column=0, sticky='nsew', padx=5, pady=5)        
        self.frame_participantlist.grid(row=0, column=1, rowspan=2, sticky='nsew', padx=5, pady=5)
        self.frame_add_participants.grid(row=2, column=0, columnspan=2, sticky='nsew', padx=5, pady=5)
        
            
        
        

if __name__ == '__main__':
    root = tk.Tk()
    root.title('Redeliste')
    
    app = App(root)
        
    # load participants.txt, if available
    filename = 'particpants.txt'
    try: 
        with open(filename, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                app.add_participant(row['name'], row['quotiert'])
                
                
                
        for selected_item in app.participantlist.get_children():
            name = app.participantlist.item(selected_item)["values"][0]
            #print(app.participantlist.item(selected_item)["values"][0])
            if(app._speakerlist_add_is_allowed_action(name)):
                quota = app.participantlist.item(selected_item)["values"][1]
                if(quota == 1):
                    new_item = app.speakerlist.insert('', 'end', text=app.participantlist.item(selected_item)["text"], values=[0] + app.participantlist.item(selected_item)["values"], tags=('quotiert'))
                    print("hinzugefügte Person ist quotiert")
                    app._speakerlist_fix_quota_action(new_item)
                else:
                    new_item = app.speakerlist.insert('', 'end', text=app.participantlist.item(selected_item)["text"], values=[0] + app.participantlist.item(selected_item)["values"])
                app.speakerlist.selection_set(new_item)
                app._speakerlist_set_position_action()
                app.root.after(1000,lambda: app._speakerlist_remove_selection_action())
    except: pass
            
    root.mainloop()
   
